import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import '../imports/startup/accounts-config.js';

// body/1
import './main.html';

// body/2 (ordered by meteor)
import '../imports/ui/layouts/body.js';
import '../imports/ui/stylesheets/body.css';


// logic of - body/1

Template.hello_counter.onCreated(function() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello_counter.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello_counter.events({
  'click button.b-clickme'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
