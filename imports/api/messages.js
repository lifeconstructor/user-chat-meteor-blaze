import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Messages = new Mongo.Collection('messages');

// Allow logged user's methods for Messages collection
Meteor.methods({

  'messages.insert'(text) {
    check(text, String);

    // Make sure the user is logged in before inserting a message
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    let curUser = Meteor.users.findOne(this.userId);

    Messages.insert({
      text,
      createdAt: new Date(),
      owner: this.userId,
      checked: false,
      username: curUser.username,
      location: curUser.profile.location,
    });
  },

  'messages.remove'(messageId) {
    check(messageId, String);

    Messages.remove(messageId);
  },

  'messages.setChecked'(messageId, setChecked) {
    check(messageId, String);
    check(setChecked, Boolean);

    Messages.update(messageId, { $set: { checked: setChecked } });
  },

  'messages.updateLocation'(location) {
    check(location, String);
    var usern = Meteor.users.findOne(this.userId).username;
    check(usern, String);

    let mesgs = Messages.find({
      $and : [ {username: { $eq: usern }}, {location: { $eq: '' }}]
    });

    mesgs.forEach(function(Message){
      Messages.update(Message._id, { $set: { location: location } });
    });
  },
});
