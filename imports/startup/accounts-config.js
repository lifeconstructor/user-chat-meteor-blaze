import { Accounts } from 'meteor/accounts-base';

// on client
if (Meteor.isClient) {
  Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY',
  });
}

// on server
if (Meteor.isServer) {
  Meteor.users.allow({
  	  update: function () { return true; },
  });
  Accounts.onCreateUser(function(options, user) {
      user.profile = {
        'fullName' : '',
        'email' : '',
        'location' : '',
      };
      return user;
  });
}
