import { Template } from 'meteor/templating';
import { Messages } from '../../api/messages.js';
import { ReactiveDict } from 'meteor/reactive-dict';
import '../components/message.js';
import '../components/users.js';
import './body.html';

/* App reactive vars */
Template.body.onCreated(function bodyOnCreated() {
  this.state = new ReactiveDict();
});

/* Users (collection) Form logic */
//http://stackoverflow.com/questions/22676470/how-to-display-meteor-user-profile-data-in-the-view
Template.body.events({
  'submit .user-profile'(event) {
      // Prevent default browser form submit
      event.preventDefault();

      // Get value from form element
      const target    = event.target;
      const fullName  = target.fullName.value;
      const email     = target.email.value;
      const location  = target.location_select.value;

      Meteor.users.update( Meteor.userId() , {
        $set : { profile : {
          'fullName'  : fullName,
          'email'     : email,
          'location'  : location,
        }}
      });
      // update messages with empty user's location
      Meteor.call('messages.updateLocation', location);
    },
    // user profile
    'change .profile-on-off input'(event, instance) {
      instance.state.set('profileOnOff', event.target.checked);
    },
});

/* Messages (collection) Form logic */
Template.body.events({
  'submit .new-message'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;

    // Insert a message into the collection
    Meteor.call('messages.insert', text);

    // Clear form
    target.text.value = '';
  },

  // hide read messages
  'change .hide-completed input'(event, instance) {
    instance.state.set('hideCompleted', event.target.checked);
  },
});

Template.body.helpers({
  messages() {
    // only for logged users
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    const instance = Template.instance();
    const loc = Meteor.users.findOne(Meteor.userId()).profile.location;

    if (instance.state.get('hideCompleted')) {
      // If hide completed is checked, filter messages
      let msgs = Messages.find(
        { $and : [ {checked: { $ne: true }}, {location: { $eq: loc }} ] },
        { sort: { createdAt: -1 } }
      );
      return msgs;
    } else {
      // Otherwise, return all of the messages
      return Messages.find({location: { $eq: loc }}, { sort: { createdAt: -1 } });
    }

  },
  incompleteCount() {
    // only for logged users
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    return Messages.find({ checked: { $ne: true } }).count();
  },
  // User
  profileOnOff() {
    // only for logged users
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    const instance = Template.instance();
    return instance.state.get('profileOnOff');
  },
});
