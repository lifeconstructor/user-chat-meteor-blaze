import { Template } from 'meteor/templating';
import { Messages } from '../../api/messages.js';
import './message.html';


// message list logic
Template.message.helpers({
  prefix_zero(hour_or_minute) {
    return parseInt(hour_or_minute) < 10 ? '0'+hour_or_minute : hour_or_minute;
  },
});

Template.message.events({
  'click .toggle-checked'() {
    // Set the checked property to the opposite of its current value
    Meteor.call('messages.setChecked', this._id, !this.checked);
  },
  'click .delete'() {
    // NOTE: can't use collection methods when "insecure" removed
    //Messages.remove(this._id);

    // method call as others, when "insecure" removed
    Meteor.call('messages.remove', this._id);
  },
});
