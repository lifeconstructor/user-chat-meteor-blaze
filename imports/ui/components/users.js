import { Template } from 'meteor/templating';
import './users.html';

// message list logic
Template.users.helpers({
  userName: function() {
    return Meteor.user().username
  },
  fullName: function() {
    return Meteor.user().profile.fullName;
  },
  email: function() {
    return Meteor.user().profile.email;
  },
  location: function() {
    return Meteor.user().profile.location;
  },
  locations: function(){
        return ["Dnipro", "Kiev", "Kharkov", "Lviv", "Odessa"];
    }
});

Template.users.events({
    "change #location_select": function (event, template) {
        var location = $(event.currentTarget).val();
        console.log("location : " + location);
    },
});
