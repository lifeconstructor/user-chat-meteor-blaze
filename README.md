"User Chat" (Meteor with Blaze)

Description:

	- User can register via email/password

	- User can sign in and edit his profile's email, password, name and select location from predefined list

	- User can broadcast message, and all users within his current location will see it

	- In case user changes his location his old messages will remain visible to the previous location and won't move to a new one

	- Autopublish and insecure packages avoided

Installation:
1. Download the project and "cd /to/project" 
2. meteor npm install
3. meteor